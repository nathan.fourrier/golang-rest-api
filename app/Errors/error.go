package Errors

import "fmt"

type CustomError struct {
    data string
}
 
func (e *CustomError) Error() string {
    return fmt.Sprintf("Error occured due to... %s", e.data)
}

