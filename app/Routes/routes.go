package Routes

import (
	"golangapi/app/Controllers"

	"github.com/gin-gonic/gin"
)

//Configure routes
func SetupRouter() *gin.Engine {
	r := gin.Default()	
	group := r.Group("/restapi")
	{
		group.GET("user", Controllers.GetAllUsers)
		group.POST("user", Controllers.CreateUser)
		group.GET("user/:email", Controllers.GetUserByEmail)
		group.PUT("user/:email", Controllers.UpdateUserByEmail)
		group.DELETE("user/:email", Controllers.DeleteUserByEmail)
	}
	return r
}
