package Models

type User struct {
	Email string `json:"email", gorm:"primarykey"`
	Phone string `json:"phone"`
	Password string `json:"password"`
}

func (b *User) TableName() string {
	return "user"
}